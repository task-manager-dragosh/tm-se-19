package ru.dragosh.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.dragosh.tm.api.UserService;
import ru.dragosh.tm.entity.User;
import ru.dragosh.tm.repository.UserRepository;

import javax.persistence.EntityManager;
import java.util.List;

@Component
@Scope(scopeName = "singleton")
@Transactional
public class UserServiceImplement implements UserService {
    @NotNull
    @Autowired
    private UserRepository userRepository;

    @Nullable
    @Override
    public User find(@NotNull final String login,
                     @NotNull final String password) {
        if (login == null || login.isEmpty())
            return null;
        if (password == null || password.isEmpty())
            return null;
        User user = userRepository.findByLoginAndPassword(login, password);
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        if (login == null || login.isEmpty())
            return null;
        User user = userRepository.findByLogin(login);
        return user;
    }

    @Override
    public User findById(@NotNull final String id) {
        User user = userRepository.getById(id);
        return user;
    }

    @Override
    public void persist(@NotNull final User user) {
        if (user == null)
            return;
        userRepository.save(user);
    }

    @Override
    public void merge(@NotNull final User user) {
        if (user == null)
            return;
        userRepository.save(user);
    }

    @Override
    public void remove(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return;
        userRepository.deleteById(userId);
    }

    @NotNull
    @Override
    public List<User> getEntitiesList() {
        List<User> list = userRepository.getEntitiesList();
        return list;
    }

    @Override
    public void loadEntities(@NotNull final List<User> entities) {
        if (entities == null)
            return;
        for (User user: entities) {
            userRepository.save(user);
        }
    }
}
